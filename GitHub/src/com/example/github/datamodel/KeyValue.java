package com.example.github.datamodel;

/**
 * Created by Kim on 2/14/15.
 */
public class KeyValue {

    private String key;
    private String value;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
