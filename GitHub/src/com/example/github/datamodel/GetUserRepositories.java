package com.example.github.datamodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by Kim on 2/14/15.
 */
public class GetUserRepositories {

    @JsonProperty("GetUserRepositories")
    private ArrayList<RepoModel> userRepositories;

    public ArrayList<RepoModel> getUserRepositories(){
        return this.userRepositories;
    }
    public void setUserRepositories(ArrayList<RepoModel> userRepositories){
        this.userRepositories = userRepositories;
    }
}
