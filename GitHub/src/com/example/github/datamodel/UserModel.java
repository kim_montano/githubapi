
package com.example.github.datamodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel {
    private String avatar_url;
    private String bio;
    private String blog;
    private String company;
    private String created_at;
    private String email;
    private String events_url;
    private Number followers;
    private String followers_url;
    private Number following;
    private String following_url;
    private String gists_url;
    private String gravatar_id;
    private boolean hireable;
    private String html_url;
    private Number id;
    private String location;
    private String login;
    private String name;
    private String organizations_url;
    private Number public_gists;
    private Number public_repos;
    private String received_events_url;
    private String repos_url;
    private boolean site_admin;
    private String starred_url;
    private String subscriptions_url;
    private String type;
    private String updated_at;
    private String url;


    @JsonProperty("avatar_url")
 	public String getAvatar_url(){
		return this.avatar_url;
	}
	public void setAvatar_url(String avatar_url){
		this.avatar_url = avatar_url;
	}

    @JsonProperty("bio")
    public String getBio(){
        return this.bio;
    }
    public void setBio(String bio){
        this.bio = bio;
    }

    @JsonProperty("blog")
    public String getBlog(){
        return this.blog;
    }
    public void setBlog(String blog){
        this.blog = blog;
    }

    @JsonProperty("company")
    public String getCompany(){
        return this.company;
    }
    public void setCompany(String company){
        this.company = company;
    }

    @JsonProperty("created_at")
 	public String getCreated_at(){
		return this.created_at;
	}
	public void setCreated_at(String created_at){
		this.created_at = created_at;
	}

    @JsonProperty("email")
    public String getEmail(){
        return this.email;
    }
    public void setEmail(String email){
        this.email = email;
    }

    @JsonProperty("events_url")
 	public String getEvents_url(){
		return this.events_url;
	}
	public void setEvents_url(String events_url){
		this.events_url = events_url;
	}

    @JsonProperty("followers")
 	public Number getFollowers(){
		return this.followers;
	}
	public void setFollowers(Number followers){
		this.followers = followers;
	}

    @JsonProperty("followers_url")
 	public String getFollowers_url(){
		return this.followers_url;
	}
	public void setFollowers_url(String followers_url){
		this.followers_url = followers_url;
	}

    @JsonProperty("following")
 	public Number getFollowing(){
		return this.following;
	}
	public void setFollowing(Number following){
		this.following = following;
	}

    @JsonProperty("following_url")
 	public String getFollowing_url(){
		return this.following_url;
	}
	public void setFollowing_url(String following_url){
		this.following_url = following_url;
	}

    @JsonProperty("gists_url")
 	public String getGists_url(){
		return this.gists_url;
	}
	public void setGists_url(String gists_url){
		this.gists_url = gists_url;
	}

    @JsonProperty("gravatar_id")
 	public String getGravatar_id(){
		return this.gravatar_id;
	}
	public void setGravatar_id(String gravatar_id){
		this.gravatar_id = gravatar_id;
	}

    @JsonProperty("hireable")
    public boolean getHireable(){
        return this.hireable;
    }
    public void setHireable(boolean hireable){
        this.hireable = hireable;
    }

    @JsonProperty("html_url")
 	public String getHtml_url(){
		return this.html_url;
	}
	public void setHtml_url(String html_url){
		this.html_url = html_url;
	}

    @JsonProperty("id")
 	public Number getId(){
		return this.id;
	}
	public void setId(Number id){
		this.id = id;
	}

    @JsonProperty("location")
    public String getLocation(){
        return this.location;
    }
    public void setLocation(String location){
        this.location = location;
    }

    @JsonProperty("login")
 	public String getLogin(){
		return this.login;
	}
	public void setLogin(String login){
		this.login = login;
	}

    @JsonProperty("name")
    public String getName(){
        return this.name;
    }
    public void setName(String name){
        this.name = name;
    }

    @JsonProperty("organizations_url")
 	public String getOrganizations_url(){
		return this.organizations_url;
	}
	public void setOrganizations_url(String organizations_url){
		this.organizations_url = organizations_url;
	}

    @JsonProperty("public_gists")
 	public Number getPublic_gists(){
		return this.public_gists;
	}
	public void setPublic_gists(Number public_gists){
		this.public_gists = public_gists;
	}

    @JsonProperty("public_repos")
 	public Number getPublic_repos(){
		return this.public_repos;
	}
	public void setPublic_repos(Number public_repos){
		this.public_repos = public_repos;
	}

    @JsonProperty("received_events_url")
 	public String getReceived_events_url(){
		return this.received_events_url;
	}
	public void setReceived_events_url(String received_events_url){
		this.received_events_url = received_events_url;
	}

    @JsonProperty("repos_url")
 	public String getRepos_url(){
		return this.repos_url;
	}
	public void setRepos_url(String repos_url){
		this.repos_url = repos_url;
	}

    @JsonProperty("site_admin")
 	public boolean getSite_admin(){
		return this.site_admin;
	}
	public void setSite_admin(boolean site_admin){
		this.site_admin = site_admin;
	}

    @JsonProperty("starred_url")
 	public String getStarred_url(){
		return this.starred_url;
	}
	public void setStarred_url(String starred_url){
		this.starred_url = starred_url;
	}

    @JsonProperty("subscriptions_url")
 	public String getSubscriptions_url(){
		return this.subscriptions_url;
	}
	public void setSubscriptions_url(String subscriptions_url){
		this.subscriptions_url = subscriptions_url;
	}

    @JsonProperty("type")
 	public String getType(){
		return this.type;
	}
	public void setType(String type){
		this.type = type;
	}

    @JsonProperty("updated_at")
 	public String getUpdated_at(){
		return this.updated_at;
	}
	public void setUpdated_at(String updated_at){
		this.updated_at = updated_at;
	}

    @JsonProperty("url")
 	public String getUrl(){
		return this.url;
	}
	public void setUrl(String url){
		this.url = url;
	}
}
