package com.example.github.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.github.datamodel.RepoModel;
import com.example.github.utilities.CurrentInformation;
import com.example.github.R;
import com.example.github.activities.RepositoryDetailsActivity;

import java.util.ArrayList;

/**
 * Created by Kim on 2/15/15.
 */
public class RepositoriesAdapter extends BaseAdapter {


    private Activity activity;
    private LayoutInflater lInflater;
    private ArrayList<RepoModel> repositories;


    public RepositoriesAdapter(ArrayList<RepoModel> repositories, Activity activity) {
        this.repositories = repositories;
        this.activity = activity;
        this.lInflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.repositories.size();
    }

    @Override
    public Object getItem(int position) {
        return repositories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Initialize
        ViewHolder vHolder = new ViewHolder();
        convertView = lInflater.inflate(R.layout.repositories_item, null);

        vHolder.tvRepositoryName = (TextView) convertView.findViewById(R.id.tvRepositoryName);
        vHolder.btnView = (TextView) convertView.findViewById(R.id.btnView);

        RepoModel selectedRepo = repositories.get(position);
        //Set Values
        vHolder.tvRepositoryName.setText(selectedRepo.getName());
        vHolder.btnView.setOnClickListener(returnViewOnClickListener(selectedRepo));

        return convertView;
    }

    //Custom Methods
    private View.OnClickListener returnViewOnClickListener(final RepoModel selectedRepository){
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CurrentInformation.selectedRepo = selectedRepository;
                goToRepositoryDetailsActivity();
            }
        };
    }

    private void goToRepositoryDetailsActivity(){
        //Go to Repository Details Activity
        Intent repositoryDetailsIntent = new Intent(activity, RepositoryDetailsActivity.class);
        activity.startActivity(repositoryDetailsIntent);
    }

    //Custom Class
    private static class ViewHolder {
        public TextView tvRepositoryName;
        public TextView btnView;
    }
}
