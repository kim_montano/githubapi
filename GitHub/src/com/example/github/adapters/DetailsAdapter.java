package com.example.github.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.github.R;
import com.example.github.datamodel.KeyValue;

import java.util.ArrayList;

/**
 * Created by Kim on 2/14/15.
 */
public class DetailsAdapter extends BaseAdapter {

    private ArrayList<KeyValue> detailsKVP;
    private LayoutInflater lInflater;

    public DetailsAdapter(ArrayList<KeyValue> detailsKVP, Activity activity) {
        this.detailsKVP = detailsKVP;
        this.lInflater = (LayoutInflater) activity.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return detailsKVP.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Initialize
        ViewHolder vHolder = new ViewHolder();
        convertView = lInflater.inflate(R.layout.details_item, null);

        vHolder.tvLabel = (TextView) convertView.findViewById(R.id.tvLabel);
        vHolder.tvValue = (TextView) convertView.findViewById(R.id.tvValue);

        //Set Values

        vHolder.tvLabel.setText(detailsKVP.get(position).getKey().toUpperCase() + ":");
        vHolder.tvValue.setText(detailsKVP.get(position).getValue());

        return convertView;
    }

    //Custom Class
    private static class ViewHolder {
        public TextView tvLabel;
        public TextView tvValue;
    }
}
