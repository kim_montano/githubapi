package com.example.github.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;
import com.example.github.R;
import com.example.github.adapters.DetailsAdapter;
import com.example.github.datamodel.KeyValue;
import com.example.github.enums.GitHubMethods;
import com.example.github.utilities.CurrentInformation;
import com.example.github.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by Kim on 2/15/15.
 */
public class RepositoryDetailsActivity extends Activity {

    ListView lvRepositoryDetails;
    TextView tvRepositoryName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repository_details_layout);

        initControls();
    }

    private void initControls(){
        tvRepositoryName = (TextView) findViewById(R.id.tvRepositoryName);
        tvRepositoryName.setText(CurrentInformation.selectedRepo.getName());

        lvRepositoryDetails = (ListView) findViewById(R.id.lvRepositoryDetails);

        ArrayList<KeyValue> repositoryDetails = Utilities.getDetailsKeyValue(GitHubMethods.REPOS);
        DetailsAdapter detailsAdapter = new DetailsAdapter(repositoryDetails, this);
        lvRepositoryDetails.setAdapter(detailsAdapter);
    }
}