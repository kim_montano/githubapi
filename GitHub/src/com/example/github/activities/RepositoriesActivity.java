package com.example.github.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;
import com.example.github.utilities.CurrentInformation;
import com.example.github.R;
import com.example.github.adapters.RepositoriesAdapter;

/**
 * Created by Kim on 2/15/15.
 */
public class RepositoriesActivity extends Activity {

    ListView lvRepositories;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repositories_layout);
        initControls();
    }

    private void initControls(){
        lvRepositories = (ListView) findViewById(R.id.lvRepositories);
        RepositoriesAdapter repositoriesAdapter = new RepositoriesAdapter(CurrentInformation.userRepos, this);
        lvRepositories.setAdapter(repositoriesAdapter);
    }
}