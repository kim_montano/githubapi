package com.example.github.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.example.github.R;
import com.example.github.adapters.DetailsAdapter;
import com.example.github.datamodel.KeyValue;
import com.example.github.enums.GitHubMethods;
import com.example.github.utilities.CurrentInformation;
import com.example.github.utilities.Utilities;

import java.util.ArrayList;

/**
 * Created by Kim on 2/14/15.
 */
public class UserProfileActivity extends Activity implements View.OnClickListener {

    //Android Controls

    ListView lvUserDetails;
    Button btnViewRepos;
    TextView tvUserName;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.userprofile_layout);

        initControls();
    }

    //Custom Methods
    private void initControls(){
        tvUserName = (TextView) findViewById(R.id.tvUserName);

        if(CurrentInformation.user.getName() == null) {
            tvUserName.setText(CurrentInformation.user.getLogin());
        }else{
            tvUserName.setText(CurrentInformation.user.getName());
        }

        lvUserDetails = (ListView) findViewById(R.id.lvUserDetails);

        ArrayList<KeyValue> userDetails = Utilities.getDetailsKeyValue(GitHubMethods.USERS);
        DetailsAdapter detailsAdapter = new DetailsAdapter(userDetails, this);
        lvUserDetails.setAdapter(detailsAdapter);

        btnViewRepos = (Button) findViewById(R.id.btnViewRepos);
        btnViewRepos.setOnClickListener(this);
    }



    private void goToRepositoriesActivity(){
        //Go to User Profile Activity
        Intent userProfileIntent = new Intent(this, RepositoriesActivity.class);
        startActivity(userProfileIntent);
    }

    //Implemented Methods
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnViewRepos:
                goToRepositoriesActivity();
                break;
        }
    }
}