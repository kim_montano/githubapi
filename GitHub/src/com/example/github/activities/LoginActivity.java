package com.example.github.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.github.dao.Dao;
import com.example.github.dao.DaoManager;
import com.example.github.utilities.CurrentInformation;
import com.example.github.utilities.GitHubAPI;
import com.example.github.R;
import com.example.github.enums.GitHubMethods;
import com.example.github.utilities.Utilities;

public class LoginActivity extends Activity implements View.OnClickListener {
    /**
     * Called when the activity is first created.
     */
    private String ghUserName = "";
    public ProgressDialog progress;

    //Controls
    Button goButton;
    EditText etUserName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        Dao.initWithContext(getApplicationContext());

        initControls();
    }

    private void initControls(){
        goButton = (Button) findViewById(R.id.goButton);
        goButton.setOnClickListener(this);

        etUserName = (EditText) findViewById(R.id.etUserName);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.goButton:

                ghUserName = etUserName.getText().toString().trim();

                if(DaoManager.doesUserExist(ghUserName)) {
                    CurrentInformation.user = DaoManager.getUserModel(ghUserName);
                    CurrentInformation.userRepos = DaoManager.getRepoModels(ghUserName);

                    progress = ProgressDialog.show(this, "Fetching data from database", "Please wait.", true);
                    goToUserProfile();
                }else{

                    if (Utilities.isConnected(this)) {
                        goButtonClicked();
                    } else {
                        Utilities.shortToast(this, "You are not connected to an internet connection.");
                    }
                }

                break;
        }
    }



    private void goButtonClicked(){
        if(Utilities.isConnected(this)) {
            if (ghUserName.length() > 0) {
                GitHubAPI gitHubApi = new GitHubAPI(ghUserName, this);
                gitHubApi.setMethod(GitHubMethods.USERS);
                gitHubApi.execute();

                progress = ProgressDialog.show(this, "Fetching data from github", "Please wait.", true);
            } else {
                Utilities.shortToast(this, "Please enter a username.");
            }
        }
    }

    public void goToUserProfile(){
        //Hide Progress
        dismissProgressDialog();

        //Go to User Profile Activity
        Intent userProfileIntent = new Intent(this, UserProfileActivity.class);
        startActivity(userProfileIntent);
    }

    //Fetch Repositories under entered username
    public void fetchRepositories(){
        //Fetch Repos
        GitHubAPI gitHubApi = new GitHubAPI(ghUserName,this);
        gitHubApi.setMethod(GitHubMethods.REPOS);
        gitHubApi.execute();
    }

    public void dismissProgressDialog(){
        progress.dismiss();
    }


}
