package com.example.github.utilities;

import com.example.github.datamodel.RepoModel;
import com.example.github.datamodel.UserModel;

import java.util.ArrayList;

/**
 * Created by Kim on 2/14/15.
 */
public class CurrentInformation {
    public static UserModel user;
    public static ArrayList<RepoModel> userRepos;
    public static RepoModel selectedRepo;
}
