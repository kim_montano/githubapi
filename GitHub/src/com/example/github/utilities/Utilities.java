package com.example.github.utilities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;
import com.example.github.datamodel.KeyValue;
import com.example.github.enums.GitHubMethods;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Kim on 2/15/15.
 */
public class Utilities {

    public static boolean isConnected(Activity activity) {
        boolean isConnectedWifi = false;
        boolean isConnectedMobile = false;

        ConnectivityManager cManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cManager.getAllNetworkInfo();
        for (NetworkInfo nInfo : netInfo) {
            if (nInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (nInfo.isConnected())
                    isConnectedWifi = true;
            if (nInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (nInfo.isConnected())
                    isConnectedMobile = true;
        }
        return isConnectedWifi || isConnectedMobile;
    }

    public static void shortToast(Activity currentActivity,String msg){
        Toast.makeText(currentActivity, msg, Toast.LENGTH_SHORT).show();
    }

    public static ArrayList<KeyValue> getDetailsKeyValue(GitHubMethods ghMethods){

        ArrayList<KeyValue>  detailsKVP = new ArrayList<KeyValue>();

        //Get Node Fields
        Iterator<Map.Entry<String, JsonNode>> nodeIterator = returnJsonNode(ghMethods).fields();

        //Iterate over nodes to get fields and values
        while (nodeIterator.hasNext()) {

            Map.Entry<String, JsonNode> entry = (Map.Entry<String, JsonNode>) nodeIterator.next();

            KeyValue kv = new KeyValue();
            kv.setKey(entry.getKey());
            String entryValue = "";
            entryValue += entry.getValue();

            if(entryValue.equals("null")){
                entryValue = "none";
            }

            kv.setValue(entryValue);
            detailsKVP.add(kv);
        }
        return detailsKVP;
    }

    //Convert POJO to JsonNode
    private static JsonNode returnJsonNode(GitHubMethods ghMethods){
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = null;
        switch (ghMethods){
            case USERS:
                jsonNode = mapper.valueToTree(CurrentInformation.user);
                break;

            case REPOS:
                jsonNode = mapper.valueToTree(CurrentInformation.selectedRepo);
                break;
        }

        return  jsonNode;
    }
}
