package com.example.github.utilities;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import com.example.github.activities.LoginActivity;
import com.example.github.dao.DaoManager;
import com.example.github.datamodel.GetUserRepositories;
import com.example.github.datamodel.RepoModel;
import com.example.github.datamodel.UserModel;
import com.example.github.enums.GitHubMethods;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Kim on 2/13/15.
 */

public class GitHubAPI extends AsyncTask<Void, Void, Object>{
    private final String kGitHubUrl = "https://api.github.com/";
    private final String kUserInfoUrl = "users/";
    private final String kRepoInfoUrl = "/repos";

    private GitHubMethods ghRequestMethod;

    private String ghUserName = null;
    private String ghRepoName = null;
    private Activity currentActivity = null;

    //Class Methods

    @Override
    protected Object doInBackground(Void... params) {

        String gitHubUrlCall = "";
        Object returnObj = null;

        try {
            switch (ghRequestMethod){
                case USERS:
                    gitHubUrlCall = kGitHubUrl + kUserInfoUrl + ghUserName;
                    returnObj = getJsonValueFromUser(gitHubUrlCall);
                    break;

                case REPOS:

                    gitHubUrlCall = kGitHubUrl + kUserInfoUrl + ghUserName + kRepoInfoUrl;

                    HttpResponse response = getHttpResponse(gitHubUrlCall);
                    String strResponse = getStringFromResponse(response);

                    if (strResponse.contains("Not Found")){
                        Log.d("!!!", "Not found!");
                    }else{
                        returnObj = getJSONValueFromRepos(strResponse);
                    }


                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();

        }


        return returnObj;
    }


    @Override
    protected void onPostExecute(Object result) {
        LoginActivity loginActivity = (LoginActivity)currentActivity;

        if(result != null){


            if(ghRequestMethod == GitHubMethods.USERS){
                //Set User Value
                CurrentInformation.user = (UserModel)result;

                //Fetch Repositories
                loginActivity.fetchRepositories();

                //Save UserInformation To Database
                DaoManager.saveUserToDatabase(CurrentInformation.user);
            }else{
                //Set User Repos Value
                CurrentInformation.userRepos = (ArrayList<RepoModel>)result;
                Log.d("!!!", CurrentInformation.userRepos.get(0).getName());

                //Save Multiple or One Repositories to User
                DaoManager.saveRepositoriesToDatabase(CurrentInformation.user.getLogin(),CurrentInformation.userRepos);

                //Go to User Profile
                loginActivity.goToUserProfile();
            }
        }else{
            Utilities.shortToast(currentActivity, "Please specify a correct username.");
            loginActivity.dismissProgressDialog();
        }
    }

    //Custom Methods

    //Initializer
    public GitHubAPI(String userName, Activity activity){
        this.ghUserName = userName;
        this.currentActivity = activity;
    }

    public void setMethod(GitHubMethods gitHubMethod){
        this.ghRequestMethod = gitHubMethod;
    }

    private HttpResponse getHttpResponse(String ghUrl) throws IOException {
        HttpClient httpclient = new DefaultHttpClient();
        HttpGet htpGet = new HttpGet(ghUrl);

        htpGet.setHeader("Accept:","application/vnd.github.v3+json");
        htpGet.setHeader("Content-type", "application/json");

        return httpclient.execute(htpGet);
    }

    private ArrayList<RepoModel> getJSONValueFromRepos(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String jsonWithArray = "{\"GetUserRepositories\":" +json + "}";
        GetUserRepositories userRepos = mapper.readValue(jsonWithArray, GetUserRepositories.class);
        return userRepos.getUserRepositories();
    }

    private UserModel getJsonValueFromUser(String ghUrl) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        URL jsonUrl = new URL(ghUrl);
        return mapper.readValue(jsonUrl, UserModel.class);
    }



    private String getStringFromResponse(HttpResponse response) throws IOException {
        if(response != null)
        {
            InputStream is = response.getEntity().getContent();


            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return sb.toString();
        }

        return "Error!";
    }

}
