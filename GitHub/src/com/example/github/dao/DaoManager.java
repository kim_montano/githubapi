package com.example.github.dao;

import android.util.Log;
import com.example.github.datamodel.RepoModel;
import com.example.github.datamodel.UserModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kim on 2/15/15.
 */
public class DaoManager {
    public static void saveUserToDatabase(UserModel user){

        Log.d("!!!", "User: " + user.getLogin());

        User newUser = new User();
        newUser.setAvatar_url(user.getAvatar_url());
        newUser.setBio(user.getBio());
        newUser.setBlog(user.getBlog());
        newUser.setCompany(user.getCompany());
        newUser.setCreated_at(user.getCreated_at());
        newUser.setEmail(user.getEmail());
        newUser.setEvents_url(user.getEvents_url());
        newUser.setFollowers(user.getFollowers().intValue());
        newUser.setFollowers_url(user.getFollowers_url());
        newUser.setFollowing(user.getFollowing().intValue());
        newUser.setGists_url(user.getGists_url());
        newUser.setGravatar_id(user.getGravatar_id());
        newUser.setHireable(user.getHireable());
        newUser.setHtml_url(user.getHtml_url());
        newUser.setGitHubUserId(user.getId().intValue());
        newUser.setLocation(user.getLocation());
        newUser.setLogin(user.getLogin());
        newUser.setName(user.getName());
        newUser.setOrganizations_url(user.getOrganizations_url());
        newUser.setPublic_gists(user.getPublic_gists().intValue());
        newUser.setPublic_repos(user.getPublic_repos().intValue());
        newUser.setReceived_events_url(user.getReceived_events_url());
        newUser.setRepos_url(user.getRepos_url());
        newUser.setSite_admin(user.getSite_admin());
        newUser.setStarred_url(user.getStarred_url());
        newUser.setSubscriptions_url(user.getSubscriptions_url());
        newUser.setType(user.getType());
        newUser.setUpdated_at(user.getUpdated_at());
        newUser.setUrl(user.getUrl());

        //Insert Publication
        Dao.sharedSession().getUserDao().insert(newUser);
    }





    public static void saveRepositoriesToDatabase(String login,ArrayList<RepoModel> repositories){

        User user = Dao.sharedSession().getUserDao().queryBuilder().where(UserDao.Properties.Login.eq(login)).unique();
        List repos = user.getRepoList();

        for(RepoModel repo : repositories){

            Repo newRepo = new Repo();
            newRepo.setArchive_url(repo.getArchive_url());
            newRepo.setAssignees_url(repo.getAssignees_url());
            newRepo.setBlobs_url(repo.getBlobs_url());
            newRepo.setBranches_url(repo.getBranches_url());
            newRepo.setClone_url(repo.getClone_url());
            newRepo.setCollaborators_url(repo.getCollaborators_url());
            newRepo.setComments_url(repo.getComments_url());
            newRepo.setCommits_url(repo.getCommits_url());
            newRepo.setCompare_url(repo.getCompare_url());
            newRepo.setContents_url(repo.getContents_url());
            newRepo.setContributors_url(repo.getContributors_url());
            newRepo.setCreated_at(repo.getCreated_at());
            newRepo.setDefault_branch(repo.getDefault_branch());
            newRepo.setDescription(repo.getDescription());
            newRepo.setDownloads_url(repo.getDownloads_url());
            newRepo.setEvents_url(repo.getEvents_url());
            newRepo.setFork(repo.getFork());
            newRepo.setForks_count(repo.getForks_count().intValue());
            newRepo.setForks_url(repo.getForks_url());
            newRepo.setFull_name(repo.getFull_name());
            newRepo.setGit_commits_url(repo.getGit_commits_url());
            newRepo.setGit_refs_url(repo.getGit_refs_url());
            newRepo.setGit_tags_url(repo.getGit_tags_url());
            newRepo.setGit_url(repo.getGit_url());
            newRepo.setHas_downloads(repo.getHas_downloads());
            newRepo.setHas_issues(repo.getHas_issues());
            newRepo.setHas_pages(repo.getHas_pages());
            newRepo.setHomepage(repo.getHomepage());
            newRepo.setHooks_url(repo.getHooks_url());
            newRepo.setHtml_url(repo.getHtml_url());
            newRepo.setGitHubRepositoryId(repo.getId().intValue());
            newRepo.setIssue_comment_url(repo.getIssue_comment_url());
            newRepo.setIssues_url(repo.getIssues_url());
            newRepo.setKeys_url(repo.getKeys_url());
            newRepo.setLabels_url(repo.getLabels_url());
            newRepo.setLanguage(repo.getLanguage());
            newRepo.setLanguages_url(repo.getLanguages_url());
            newRepo.setMerges_url(repo.getMerges_url());
            newRepo.setMilestones_url(repo.getMilestones_url());
            newRepo.setMirror_url(repo.getMirror_url());
            newRepo.setName(repo.getName());
            newRepo.setNotifications_url(repo.getNotifications_url());
            newRepo.setOpen_issues(repo.getOpen_issues().intValue());
            newRepo.setOpen_issues_count(repo.getOpen_issues_count().intValue());
            newRepo.setIsPrivate(repo.getPrivate());
            newRepo.setPulls_url(repo.getPulls_url());
            newRepo.setPushed_at(repo.getPushed_at());
            newRepo.setReleases_url(repo.getReleases_url());
            newRepo.setSize(repo.getSize().intValue());
            newRepo.setSsh_url(repo.getSsh_url());
            newRepo.setStargazers_count(repo.getStargazers_count().intValue());
            newRepo.setStargazers_url(repo.getStargazers_url());
            newRepo.setStatuses_url(repo.getStatuses_url());
            newRepo.setSubscribers_url(repo.getSubscribers_url());
            newRepo.setSvn_url(repo.getSvn_url());
            newRepo.setTags_url(repo.getTags_url());
            newRepo.setTeams_url(repo.getTeams_url());
            newRepo.setTrees_url(repo.getTrees_url());
            newRepo.setUpdated_at(repo.getUpdated_at());
            newRepo.setUrl(repo.getUrl());
            newRepo.setWatchers(repo.getWatchers().intValue());
            newRepo.setWatchers_count(repo.getWatchers_count().intValue());

            newRepo.setUserId(user.getId());
            Dao.sharedSession().insert(newRepo);

            repos.add(newRepo);
        }
    }

    public static Boolean doesUserExist(String login){
        if(getUser(login) == null)
            return false;
        else
            return true;
    }

    public static User getUser(String login){
        User user = Dao.sharedSession().getUserDao().queryBuilder().where(UserDao.Properties.Login.eq(login)).unique();
        return user;
    }

    public static ArrayList<RepoModel> getRepoModels(String login){
        User user = Dao.sharedSession().getUserDao().queryBuilder().where(UserDao.Properties.Login.eq(login)).unique();
        List repos = user.getRepoList();

        ArrayList<RepoModel> repoModels = new ArrayList<RepoModel>();

        for(Object repoDao : repos){
            Repo repo = (Repo)repoDao;
            RepoModel repoModel = new RepoModel();

            repoModel.setArchive_url(repo.getArchive_url());
            repoModel.setAssignees_url(repo.getAssignees_url());
            repoModel.setBlobs_url(repo.getBlobs_url());
            repoModel.setBranches_url(repo.getBranches_url());
            repoModel.setClone_url(repo.getClone_url());
            repoModel.setCollaborators_url(repo.getCollaborators_url());
            repoModel.setComments_url(repo.getComments_url());
            repoModel.setCommits_url(repo.getCommits_url());
            repoModel.setCompare_url(repo.getCompare_url());
            repoModel.setContents_url(repo.getContents_url());
            repoModel.setContributors_url(repo.getContributors_url());
            repoModel.setCreated_at(repo.getCreated_at());
            repoModel.setDefault_branch(repo.getDefault_branch());
            repoModel.setDescription(repo.getDescription());
            repoModel.setDownloads_url(repo.getDownloads_url());
            repoModel.setEvents_url(repo.getEvents_url());
            repoModel.setFork(repo.getFork());
            repoModel.setForks_count(repo.getForks_count().intValue());
            repoModel.setForks_url(repo.getForks_url());
            repoModel.setFull_name(repo.getFull_name());
            repoModel.setGit_commits_url(repo.getGit_commits_url());
            repoModel.setGit_refs_url(repo.getGit_refs_url());
            repoModel.setGit_tags_url(repo.getGit_tags_url());
            repoModel.setGit_url(repo.getGit_url());
            repoModel.setHas_downloads(repo.getHas_downloads());
            repoModel.setHas_issues(repo.getHas_issues());
            repoModel.setHas_pages(repo.getHas_pages());
            repoModel.setHomepage(repo.getHomepage());
            repoModel.setHooks_url(repo.getHooks_url());
            repoModel.setHtml_url(repo.getHtml_url());
            repoModel.setId(repo.getGitHubRepositoryId().intValue());
            repoModel.setIssue_comment_url(repo.getIssue_comment_url());
            repoModel.setIssues_url(repo.getIssues_url());
            repoModel.setKeys_url(repo.getKeys_url());
            repoModel.setLabels_url(repo.getLabels_url());
            repoModel.setLanguage(repo.getLanguage());
            repoModel.setLanguages_url(repo.getLanguages_url());
            repoModel.setMerges_url(repo.getMerges_url());
            repoModel.setMilestones_url(repo.getMilestones_url());
            repoModel.setMirror_url(repo.getMirror_url());
            repoModel.setName(repo.getName());
            repoModel.setNotifications_url(repo.getNotifications_url());
            repoModel.setOpen_issues(repo.getOpen_issues().intValue());
            repoModel.setOpen_issues_count(repo.getOpen_issues_count().intValue());
            repoModel.setPrivate(repo.getIsPrivate());
            repoModel.setPulls_url(repo.getPulls_url());
            repoModel.setPushed_at(repo.getPushed_at());
            repoModel.setReleases_url(repo.getReleases_url());
            repoModel.setSize(repo.getSize().intValue());
            repoModel.setSsh_url(repo.getSsh_url());
            repoModel.setStargazers_count(repo.getStargazers_count().intValue());
            repoModel.setStargazers_url(repo.getStargazers_url());
            repoModel.setStatuses_url(repo.getStatuses_url());
            repoModel.setSubscribers_url(repo.getSubscribers_url());
            repoModel.setSvn_url(repo.getSvn_url());
            repoModel.setTags_url(repo.getTags_url());
            repoModel.setTeams_url(repo.getTeams_url());
            repoModel.setTrees_url(repo.getTrees_url());
            repoModel.setUpdated_at(repo.getUpdated_at());
            repoModel.setUrl(repo.getUrl());
            repoModel.setWatchers(repo.getWatchers().intValue());
            repoModel.setWatchers_count(repo.getWatchers_count().intValue());

            repoModels.add(repoModel);
        }

        return repoModels;
    }

    public static UserModel getUserModel(String login){
        User user = getUser(login);

        UserModel userModel = new UserModel();

        userModel.setAvatar_url(user.getAvatar_url());
        userModel.setBio(user.getBio());
        userModel.setBlog(user.getBlog());
        userModel.setCompany(user.getCompany());
        userModel.setCreated_at(user.getCreated_at());
        userModel.setEmail(user.getEmail());
        userModel.setEvents_url(user.getEvents_url());
        userModel.setFollowers(user.getFollowers().intValue());
        userModel.setFollowers_url(user.getFollowers_url());
        userModel.setFollowing(user.getFollowing().intValue());
        userModel.setGists_url(user.getGists_url());
        userModel.setGravatar_id(user.getGravatar_id());
        userModel.setHireable(user.getHireable());
        userModel.setHtml_url(user.getHtml_url());
        userModel.setId(user.getGitHubUserId().intValue());
        userModel.setLocation(user.getLocation());
        userModel.setLogin(user.getLogin());
        userModel.setName(user.getName());
        userModel.setOrganizations_url(user.getOrganizations_url());
        userModel.setPublic_gists(user.getPublic_gists().intValue());
        userModel.setPublic_repos(user.getPublic_repos().intValue());
        userModel.setReceived_events_url(user.getReceived_events_url());
        userModel.setRepos_url(user.getRepos_url());
        userModel.setSite_admin(user.getSite_admin());
        userModel.setStarred_url(user.getStarred_url());
        userModel.setSubscriptions_url(user.getSubscriptions_url());
        userModel.setType(user.getType());
        userModel.setUpdated_at(user.getUpdated_at());
        userModel.setUrl(user.getUrl());


        return userModel;
    }

}
