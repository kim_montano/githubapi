package com.example.github.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by whitecloak on 5/21/14.
 */
public class Dao {


    /**
     * Methods for Singleton
     */

    private static Dao sharedDao = null;
    private static DaoSession sharedSession = null;


    public static Dao initWithContext(Context context) {
        if (sharedDao == null) {
            sharedDao = new Dao(context);
        }

        return sharedDao;
    }


    public static DaoSession sharedSession() {
        if (sharedSession == null) {
            Log.e("", "Dao has not been initialized");
        }

        return sharedSession;
    }


    /**
     * Methods for Dao
     */

    private Context mContext;
    private DaoMaster.DevOpenHelper mHelper;
    private SQLiteDatabase mDataBase;
    private DaoMaster mDaoMaster;
    private DaoSession mDaoSession;


    public Dao(Context context) {
        mContext = context;

        mHelper = new DaoMaster.DevOpenHelper(mContext, "github-db", null);
        mDataBase = mHelper.getWritableDatabase();
        mDaoMaster = new DaoMaster(mDataBase);
        mDaoSession = mDaoMaster.newSession();

        Dao.sharedSession = mDaoSession;
    }
}