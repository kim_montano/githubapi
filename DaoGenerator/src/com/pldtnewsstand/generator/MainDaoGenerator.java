/*
 * Copyright (C) 2011 Markus Junginger, greenrobot (http://greenrobot.de)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.pldtnewsstand.generator;

import de.greenrobot.daogenerator.*;

/**
 * Generates entities and DAOs for the example project DaoExample.
 * <p/>
 * Run it as a Java application (not Android).
 *
 * @author Markus
 */
public class MainDaoGenerator {


    public static void main(String[] args) throws Exception {

        Schema schema = new Schema(6, "com.example.github.dao");

        // Publication

        Entity user = schema.addEntity("User");

        user.addIdProperty();
        user.addStringProperty("avatar_url");
        user.addStringProperty("bio");
        user.addStringProperty("blog");
        user.addStringProperty("company");
        user.addStringProperty("created_at");
        user.addStringProperty("email");
        user.addStringProperty("events_url");
        user.addIntProperty("followers");
        user.addStringProperty("followers_url");
        user.addIntProperty("following");
        user.addStringProperty("following_url");
        user.addStringProperty("gists_url");
        user.addStringProperty("gravatar_id");
        user.addBooleanProperty("hireable");
        user.addStringProperty("html_url");
        user.addIntProperty("gitHubUserId");
        user.addStringProperty("location");
        user.addStringProperty("login");
        user.addStringProperty("name");
        user.addStringProperty("organizations_url");
        user.addIntProperty("public_gists");
        user.addIntProperty("public_repos");
        user.addStringProperty("received_events_url");
        user.addStringProperty("repos_url");
        user.addBooleanProperty("site_admin");
        user.addStringProperty("starred_url");
        user.addStringProperty("subscriptions_url");
        user.addStringProperty("type");
        user.addStringProperty("updated_at");
        user.addStringProperty("url");

        Entity repo = schema.addEntity("Repo");

        repo.addIdProperty();
        repo.addStringProperty("archive_url");
        repo.addStringProperty("assignees_url");
        repo.addStringProperty("blobs_url");
        repo.addStringProperty("branches_url");
        repo.addStringProperty("clone_url");
        repo.addStringProperty("collaborators_url");
        repo.addStringProperty("comments_url");
        repo.addStringProperty("commits_url");
        repo.addStringProperty("compare_url");
        repo.addStringProperty("contents_url");
        repo.addStringProperty("contributors_url");
        repo.addStringProperty("created_at");
        repo.addStringProperty("default_branch");
        repo.addStringProperty("description");
        repo.addStringProperty("downloads_url");
        repo.addStringProperty("events_url");
        repo.addBooleanProperty("fork");
        repo.addIntProperty("forks");
        repo.addIntProperty("forks_count");
        repo.addStringProperty("forks_url");
        repo.addStringProperty("full_name");
        repo.addStringProperty("git_commits_url");
        repo.addStringProperty("git_refs_url");
        repo.addStringProperty("git_tags_url");
        repo.addStringProperty("git_url");
        repo.addBooleanProperty("has_downloads");
        repo.addBooleanProperty("has_issues");
        repo.addBooleanProperty("has_pages");
        repo.addBooleanProperty("has_wiki");
        repo.addStringProperty("homepage");
        repo.addStringProperty("hooks_url");
        repo.addStringProperty("html_url");
        repo.addIntProperty("gitHubRepositoryId");
        repo.addStringProperty("issue_comment_url");
        repo.addStringProperty("issue_events_url");
        repo.addStringProperty("issues_url");
        repo.addStringProperty("keys_url");
        repo.addStringProperty("labels_url");
        repo.addStringProperty("language");
        repo.addStringProperty("languages_url");
        repo.addStringProperty("merges_url");
        repo.addStringProperty("milestones_url");
        repo.addStringProperty("mirror_url");
        repo.addStringProperty("name");
        repo.addStringProperty("notifications_url");
        repo.addIntProperty("open_issues");
        repo.addIntProperty("open_issues_count");
        repo.addBooleanProperty("isPrivate");
        repo.addStringProperty("pulls_url");
        repo.addStringProperty("pushed_at");
        repo.addStringProperty("releases_url");
        repo.addIntProperty("size");
        repo.addStringProperty("ssh_url");
        repo.addIntProperty("stargazers_count");
        repo.addStringProperty("stargazers_url");
        repo.addStringProperty("statuses_url");
        repo.addStringProperty("subscribers_url");
        repo.addStringProperty("subscription_url");
        repo.addStringProperty("svn_url");
        repo.addStringProperty("tags_url");
        repo.addStringProperty("teams_url");
        repo.addStringProperty("trees_url");
        repo.addStringProperty("updated_at");
        repo.addStringProperty("url");
        repo.addIntProperty("watchers");
        repo.addIntProperty("watchers_count");

        //Create One-to-many Relationship
        Property userId = repo.addLongProperty("userId").getProperty();
        repo.addToOne(user,userId);
        user.addToMany(repo, userId);

        new DaoGenerator().generateAll(schema, "../GitHub/src");
    }


}
